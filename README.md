# ProductPlacementAR

Repo contains Unity package for recreation of experiment detailed in my final year thesis, due to be hosted in this repo after submission. Already included is the GoogleVR SDK for use with a google cardboard device

# How to use
## Importing the package

The package imports into a unity environment the same as any other package, simply import it as a custom package using the following instructions from the Unity website.

You can import custom packages which have been exported from your own projects or from projects made by other Unity users.

To import a new custom package:

Open the project you want to import assets into.
Choose Assets > Import Package > Custom Package… to bring up up File Explorer (Windows) or Finder (Mac).
Select the package you want from Explorer or Finder, and the Import Unity Package dialog box displays, with all the items in the package pre-checked, ready to install.
Select Import and Unity puts the contents of the package into the Assets folder, which you can access from your Project View.
# Aspects of the package
The following section gives a quick description of what is included in each of the scenes, details on the intricacies of Unity is not included as it is assumed these can be obtained from the Unity website.
## Scenes
The first two trial scenes "1trial" and "2trial" are the skeleton for all builds. If it is the control condition "3trial" is run twice else "bottle" or "poster" followed by "3trial". This maintains that all three trials see the same cube manipulations.
### 1trial
This scene contains the room as normal with the cubes placed around the room. The empty GameObject main has assigned "Main1" which then controls the appearance and disappearance of the cubes in succession.
### 2trial
This scene contains the room as normal with the cubes placed around the room. The empty GameObject main has assigned "Main2" which then controls the appearance and disappearance of the cubes in succession.
### 3trial
This scene contains the room as normal with the cubes placed around the room. The empty GameObject main has assigned "Main3" which then controls the appearance and disappearance of the cubes in succession.
### bottle
This scene contains the room with the addition of the Buxton water bottle on the desk. The selected placement and dimensions of the water bottle were chosen and tested thoroughly to make sure the brand was distinguishable in VR. The empty GameObject main has assigned "Main3" which then controls the appearance and disappearance of the cubes in succession exactly the same as in "3trial".
### poster
This scene contains the room with the addition of the poster of Evian on the wall. The material of a [poster model](https://3dwarehouse.sketchup.com/model/uc834573f-7dca-4f05-a015-e6ccf4cb0c5e/Framed-Vintage-1960s-Rock-Poster) was replaced with a Evian [poster](https://s-media-cache-ak0.pinimg.com/736x/eb/02/08/eb0208881f11eeb875032ca445203cd7.jpg). The empty GameObject main has assigned "Main3" which then controls the appearance and disappearance of the cubes in succession exactly the same as in "3trial".
### familiarisation
Contains only the static model with no cubes, no assigned scripts. This was used to familiarise the subject with the environment that represents the room where the trial would be taking place.
### test
This is scene created to clarify the potential differences to be noted in the cubes.
## Models

### simplebedroom
Simple bedroom was created on sketchup and is an altered version of a [bedroom](https://3dwarehouse.sketchup.com/model/4dfe556040204452d6bd50bf5a8d0197/Bedroom) imported from 3d Warehouse. Additional models used include a [bed](https://3dwarehouse.sketchup.com/model/aea10d85483a493fe25194f3c5a4f307/Boys-bed) and a [lamp](https://3dwarehouse.sketchup.com/model/e98c05f4cc8c7afcf648915c85184f8c/Globe-hanging-lamp). It was then all texturised to give it a more realistic feel
### bottle
This model was an altered version of a previous [model](https://3dwarehouse.sketchup.com/model.html?id=1ffd7113492d375593202bf99dddc268) re-textured appropriately.
### poster
As mentioned above, this poster is a re-textured [poster](https://s-media-cache-ak0.pinimg.com/736x/eb/02/08/eb0208881f11eeb875032ca445203cd7.jpg)
## Scripts
The only included scripts are main1 main2 and main3. Theses are assigned to empty GameObjects to control the appearance and disappearance of the cubes in the room. The basic body of the scripts is the same. The few alterations are the manipulations of the dimensions of the cubes and the level to be loaded afterwards.
